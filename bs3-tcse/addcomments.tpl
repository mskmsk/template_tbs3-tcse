<div class="col-xs-12 col-sm-12 col-md-7 col-lg-6">
	<ul class="list-group">
		[not-logged]
		<li class="list-group-item">
			<input placeholder="Ваше имя" type="text" name="name" class="form-control">
		</li>
		<li class="list-group-item">
			<input placeholder="E-mail" type="mail" name="mail" class="form-control">
		</li>
		[/not-logged]
		<li class="list-group-item editorcomm">
			{editor}
		</li>
		[question]
		<li class="list-group-item">
			<div><b>Вопрос:</b></div>
			<p>{question}</p>
		</li>
		<li class="list-group-item">
			<div><b>Ответ:</b> <span class="impot">*</span></div>
			<input type="text" name="question_answer" id="question_answer" placeholder="укажите ответ на вопрос" class="form-control" />
		</li>
		[/question]
		[sec_code]
		<li class="list-group-item">
			<div class="c-captcha-box">
				<label for="sec_code">Повторите код:</label>
				<div class="c-captcha">
					<p>{sec_code}</p>
					<p><input title="Введите код указанный на картинке" type="text" name="sec_code" id="sec_code" class="form-control" placeholder="сюда вводите код"></p>
				</div>
			</div>
		</li>
		[/sec_code]
		[recaptcha]
		<li class="list-group-item">
			<div>Введите слова</div>
			{recaptcha}
		</li>
		[/recaptcha]
	</ul>
	<div>
		<p></p>
		<button type="submit" name="submit" class="btn btn-success btn-lg btn-block">
			<span>
				[not-aviable=comments]<i class="fa fa-cloud-upload"></i> Отправить[/not-aviable]
				[aviable=comments]<i class="fa fa-eraser"></i> Изменить[/aviable]
			</span>
		</button>
	</div>
</div>
