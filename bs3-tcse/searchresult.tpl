[searchposts]
[fullresult]
	
<div class="col-xs-12 col-sm-12 col-md-12">
	<div class="row">

		<div class="panel panel-default">
			<div class="panel-body">

				<a href="{full-link}" class="text-muted"><h2>{title}</h2></a>

				<p class="visible-md visible-sm visible-xs">
					<i class="fa fa-folder"></i> {link-category}&emsp;
					<i class="fa fa-calendar"></i> [day-news]{date}[/day-news]&emsp;
					[com-link]<i class="fa fa-comments-o"></i>  {comments-num}&emsp;[com-link]
					<i class="fa fa-eye"></i>  {views}
				</p>

				[image-1]
					<div class="visible-lg">
						<a href="{full-link}"><img src="{image-1}" class="img-rounded img-responsive" alt="" /></a>
					</div>

					<div class="visible-sm visible-md">
						<a href="{full-link}"><img src="{image-1}" class="img-rounded img-responsive col-sm-3" alt="" /></a>
					</div>

					<div class="visible-xs col-xs-12">
						<p>
							<a href="{full-link}"><center><img src="{image-1}" class="img-rounded img-responsive" alt="" /></center></a>
						</p>
					</div>
				[/image-1]

				<p>
					{short-story limit="300"}...
				</p>
				[tags]
					<p>
						<i class="fa fa-tags"></i> {tags}
					</p>
				[/tags]
				
					[edit]
						<button class="btn btn-default" type="button">Редактировать</button>
					[/edit]
				
				<div class="hidden-xs">
					<p class="text-right">
						<a class="btn btn-primary" href="{full-link}">подробнее &raquo;</a>
					</p>
				</div>

				<div class="visible-xs">
					<p>
						<a class="btn btn-primary btn-block" href="{full-link}">подробнее &raquo;</a>
					</p>
				</div>

			</div>
		</div>

	</div>
</div>


[/fullresult]
[/searchposts]
