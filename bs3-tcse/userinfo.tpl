<div class="row">
<div class="col-md-12">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#user" data-toggle="tab">Пользователь: {usertitle}</a></li>
[not-logged]
    <li><a href="#edituser" data-toggle="tab">Редактирование информации</a></li>
[/not-logged]
    </ul>

<div class="tab-content">
    <div class="tab-pane active" id="user">
<p>
  <div class="panel panel-default">
  <div class="panel-body">

[fullname]Полное имя: {fullname}<br />[/fullname]
Дата регистрации: {registration}<br />
Последнее посещение: {lastdate}&nbsp;[online]<img src="{THEME}/images/online.png" style="vertical-align: middle;" title="Пользователь Онлайн" alt="Пользователь Онлайн" />[/online][offline]<img src="{THEME}/images/offline.png" style="vertical-align: middle;" title="Пользователь offline" alt="Пользователь offline" />[/offline]<br />
Группа:    <font color="red">{status}</font>[time_limit] в группе до: {time_limit}[/time_limit]<br />

[not-logged]
<p>
  [land]Место жительства: {land}<br />[/land]

  <br />Немного о себе:<br /><i>{info}</i><br />
</p>
[/not-logged]

[news-num]
<p>
Количество публикаций:     <span class="badge">{news-num}</span> <button type="button" class="btn btn-default"> {news} </button> [rss]<i class="fa fa-rss-square"></i>[/rss]<br />
</p>
[/news-num]

[comm-num]
<p>
Количество комментариев: <span class="badge">{comm-num}</span>  <button type="button" class="btn btn-default">{comments} </button><br /><br />
</p>
[/comm-num]
<p>
<button type="button" class="btn btn-default"><i class="fa fa-envelope"></i> {email} </button>&nbsp;
<button type="button" class="btn btn-default"><i class="fa fa-comment"></i> {pm}</button>&nbsp;
</p>

</div>
</div>
</p>
    </div>

[not-logged]
    <div class="tab-pane" id="edituser">
      <p>

      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="120" height="25">Ваш E-Mail:</td>
                              <td ><input type="text" name="email" value="{editmail}" class="f_input" /></td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td>{hidemail}</td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                            </tr>
                            <tr>
                              <td height="25">Ваше Имя:</td>
                              <td><input type="text" name="fullname" value="{fullname}" class="f_input" /></td>
                            </tr>
                            <tr>
                              <td height="25"><nobr>Место жительства:  </nobr></td>
                              <td><input type="text" name="land" value="{land}" class="f_input" /></td>
                            </tr>
                            <tr>
                              <td height="25">Номер ICQ:</td>
                              <td><input type="text" name="icq" value="{icq}" class="f_input" /></td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                            </tr>
                            <tr>
                              <td height="25">Старый пароль:</td>
                              <td><input type="password" name="altpass" class="f_input" /></td>
                            </tr>
                            <tr>
                              <td height="25">Новый пароль:</td>
                              <td><input type="password" name="password1" class="f_input" /></td>
                            </tr>
                            <tr>
                              <td height="25">Повторите:</td>
                              <td><input type="password" name="password2" class="f_input" /></td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                            </tr>
                          </table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td height="25">Блокировка по IP:<br /><textarea name="allowed_ip" style="width:97%; height:70px" class="f_textarea" />{allowed-ip}</textarea><br />Ваш текущий IP: <strong>{ip}</strong><br /><br /><font style="color:red;font-size:10px;">* Внимание! Будьте бдительны при изменении данной настройки. Доступ к Вашему аккаунту будет доступен только с того IP-адреса или подсети, который Вы укажете. Вы можете указать несколько IP адресов, по одному адресу на каждую строчку.<br />Пример: 192.48.25.71 или 129.42.*.*</font></td>
                            </tr>
                          </table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                            </tr>
                            <tr>
                              <td height="25">Аватар:</td>
                              <td><input type="file" name="image" class="f_input" /></td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td><input type="checkbox" name="del_foto" value="yes" />  Удалить фотографию</td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                            </tr>
                          </table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td height="25">О себе:<br /><textarea name="info" style="width:97%; height:70px" class="f_textarea" />{editinfo}</textarea></td>
                            </tr>
                            <tr>
                              <td height="25">Подпись:<br /><textarea name="signature" style="width:97%; height:70px" class="f_textarea" />{editsignature}</textarea></td>
                            </tr>
                          </table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
{xfields}
                            <tr>
                              <td colspan="2" height="25"><div style="padding-top:2px; padding-left:0px;">
                              <input type="submit" value=" Отправить " /><br />
                              <input name="submit" type="hidden" id="submit" value="submit" />
                              </div></td>
                            </tr>
                          </table>
      
    </p>
    </div>
[/not-logged]
  </div>

</div>
</div>