<div class="row bg-grey">
	<div class="container">
		
			<div class="col-md-3 col-sm-6 col-xs-6">
				<p>
					<ul class="list-unstyled">
						<li><a href="#">Портфолио</a></li>
						<li><a href="#">Продукты</a></li>
						<li><a href="#">Проекты</a></li>
						<li><a href="#">Инструкции</a></li>
						
					</ul>
				</p>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6">
				<p>	
					<ul class="list-unstyled">
						<li><a href="#">Страница контактов</a></li>
						<li><a href="/index.php?do=feedback">Форма обратной связи</a></li>
						<li><a href="mailto:mail@tcse-cms.com">Написать email</a></li>
						<li><a href="tel:+78123092624">Позвонить +7 (812) 309-26-24</a></li>
						<li><a href="/?do=lastcomments">Лента комментариев</a></li>
					</ul>	
					
				</p>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6">
				<p>
					<ul class="list-unstyled">
						<li><a href="#">Заполнить анкету</a></li>
						<li><a href="#">Бриф на разработку сайта</a></li>
						<li><a href="#">Бриф на дизайн-макета сайта</a></li>
						<li><a href="#">Оценочная смета</a></li>
					</ul>
					            
				</p>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6">
				<p>
					<ul class="list-unstyled">
						<li><a href="#">Цены</a></li>
						<li><a href="#">Услуги</a></li>
						<li><a href="#">Абонентское обслуживание</a></li>
						<li><a href="#">Постпродажная поддержка</a></li>
					</ul>
					            
				</p>
			</div>
		
	</div>
</div>