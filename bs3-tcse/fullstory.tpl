<div class="row">
	<article>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		          <h2>{title}</h2>
		          <p>
				<i class="fa fa-folder-open-o"></i> {link-category}&emsp;
				<i class="fa fa-calendar"></i> [day-news]{date}[/day-news]&emsp;
				<i class="fa fa-eye"></i> {views}&emsp;
				[add-favorites]<i class="fa fa-bookmark-o"></i>[/add-favorites]
				[del-favorites]<i class="fa fa-bookmark"></i>[/del-favorites]&emsp;
				[edit]<i class="fa fa-edit"></i>[/edit]
		          </p>
	          	
		          	<div class="full-content">{full-story}</div>
		          	<p>&emsp; </p>
	         	
	         		<p>
	         			[tags]
	         			<div class="panel panel-default">
			  		<div class="panel-body">
	         					<i class="fa fa-tags"></i> {tags}&emsp;&emsp;[complaint]<i class="fa fa-thumbs-o-down"></i>  Настрочить жалобу в спортлото [/complaint]&emsp;&emsp;[print-link]<i class="fa fa-print"></i>  Распечатать[/print-link]
	         				</div>
	         			</div>
	         			[/tags]
	         		</p>
	         		[rating]<p>{rating}</p>[/rating]    	
		</div>
	</article>
</div>

<div class="row">
	<section>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hidden-xs">
			<div class="panel panel-warning">
				<div class="panel-heading">Реклама на сайте</div>
				<div class="panel-body">
					<p>
						сюда можно поставить код контекстной рекламы.
					</p>
				</div>
			</div>	
		</div>
	</section>

	[related-news]
	<section>
		<div class="col-lg-12 clearfix">
			<h3 class="text-success">Похожие публикации</h3>
			<ul class="list-group">
				{related-news}
			</ul>
		</div>
	</section>
	[/related-news]
	
	
</div>

<div class="row">
	<section>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			[comments]<h3>Обсуждения</h3>[/comments]

			[not-comments]
			<div class="well">
				У данной публикации еще нет комментариев. Хотите начать обсуждение?
			</div>
			[/not-comments]
			<p>	
			       <a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target="#AddcommentsModal">
			       	<i class="fa fa-pencil"></i> Добавить комментарий
			       </a>
		  	</p>
		  	<p>{comments}</p>
		  	<p>{navigation}</p>
		</div>
	</section>
</div>



<!-- Addcomments Modal -->
<div class="modal fade" id="AddcommentsModal" tabindex="-1" role="dialog"  aria-labelledby="AddcommentsModal"  aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
				<h4 class="modal-title">Форма добавления комментария</h4>
			</div>
			<div class="modal-body">
				{addcomments}
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Закрыть</button>
			</div>
		</div>
	</div>
</div><!-- Addcomments Modal -->