<p></p>
<div class="row">

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

	<div class="row">
		<div class="hidden-xs col-sm-1 col-md-1 col-lg-1">
			<img src="{foto}" class="img-responsive" alt="" />
		</div>
		<div class="col-xs-12 col-sm-11 col-md-11 col-lg-11">
			<div>
				<small><span class="badge">#{comment-id}</span> написал: <strong>{author}</strong> {news_title}<br />Группа: {group-name}<br />
				{date} {ip}</small>
			</div>	
		</div>

	</div>

	<p>
		    <div class="well smartcomments">
		    	{comment}
		    </div>
		    	[signature]
			<div class="visible-sm visible-xs">
			<br /><br />--------------------<br />{signature}
			</div>
			[/signature]
	</p>
	<p>
		{mass-action}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		[fast]<button type="button" class="btn btn-info">цитировать</button>[/fast]&nbsp;
		[com-edit]<button type="button" class="btn btn-warning">редактировать</button>[/com-edit]&nbsp; 
		[com-del]<button type="button" class="btn btn-danger">удалить</button>[/com-del]
	</p>

</div>

</div>

