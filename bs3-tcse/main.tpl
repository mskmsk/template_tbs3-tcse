<!DOCTYPE html>
<html>
	<head>
		{headers}
		<!-- Favicons -->
		<link rel="apple-touch-icon" sizes="57x57" href="{THEME}/images/favicons/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="{THEME}/images/favicons/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="{THEME}/images/favicons/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="{THEME}/images/favicons/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="{THEME}/images/favicons/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="{THEME}/images/favicons/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="{THEME}/images/favicons/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="{THEME}/images/favicons/apple-touch-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="{THEME}/images/favicons/apple-touch-icon-180x180.png">
		<link rel="icon" type="image/png" href="{THEME}/images/favicons/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="{THEME}/images/favicons/android-chrome-192x192.png" sizes="192x192">
		<link rel="icon" type="image/png" href="{THEME}/images/favicons/favicon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="{THEME}/images/favicons/favicon-16x16.png" sizes="16x16">
		<link rel="manifest" href="{THEME}/images/favicons/manifest.json">
		<link rel="shortcut icon" href="{THEME}/images/favicons/favicon.ico">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="msapplication-TileImage" content="{THEME}/images/favicons/mstile-144x144.png">
		<meta name="msapplication-config" content="{THEME}/images/favicons/browserconfig.xml">
		<meta name="theme-color" content="#ffffff">
		<!-- mobile tags -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Bootstrap -->
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" >
		<link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.5/cerulean/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">	
		<!-- DLE style -->
		<link rel="stylesheet" href="{THEME}/css/engine.css">
		<link rel="stylesheet" href="{THEME}/css/styles.css">
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		{AJAX}
		<header>
			<div class="container">     
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="header-logo">
							<a href="/"><img src="http://imgholder.ru/480x80&text=logo+site" class="img-responsive img-thumbnail" alt="logo site"></a>
						</div>
					</div>
				</div>
			</div>
			{include file="menu_header.tpl"}
		</header>

		<main>
			<div class="container"> 
				[aviable=main]
				<div class="row">
					<div class="col-xs-12 col-sm-12 visible-xs visible-sm">
						<p>
							<form class="form-inline" role="form" method="post" action=''>
								<div class="input-group">
									<input type="text" class="form-control" name="story">
									<input type="hidden" name="do" value="search">
									<input type="hidden" name="subaction" value="search">
									<span class="input-group-addon" type="submit" ><i class="fa fa-search"></i></span>
								</div>
							</form>
						</p>
					</div>
				</div>
				[/aviable]
				
				{speedbar}
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
						{info}
						{content}
					</div>
					<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
						{include file="sidebar.tpl"}
					</div>
				</div>
			</div>
		</main>

		<footer>
			{include file="menu_footer.tpl"}
			<div class="container">     
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<p>&emsp;</p>
						<p>
							© 2015 Бесплатный шаблон <b>BS3-TCSE</b> для <a href="http://dle-news.ru">DataLife Engine</a> от веб-студии <a href="http://tcse-cms.com">tcse-cms.com</a>.
						</p>
						<p>
							В основе фреймворк <a href="http://getbootstrap.com/css/">twitter bootstrap 3</a>, темы оформления  <a href="http://bootswatch.com/cerulean/">bootswatch</a> и набор иконок <a href="http://fontawesome.io/icons/">fontawesome</a>.
						</p>
						<p>&emsp;</p>
					</div>
				</div>
			</div>
		</footer>


		<!-- Login Modal -->
		<div class="modal fade" id="LoginModal" tabindex="-1" role="dialog"  aria-labelledby="LoginModal"  aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
						<h4 class="modal-title">Форма авторизации</h4>
					</div>
					<div class="modal-body">
						{login}
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-info" data-dismiss="modal"><i class="fa fa-home"></i> Ясно</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times-circle"></i> Понятно</button>
					</div>
				</div>
			</div>
		</div><!-- Login Modal -->

		<!-- Bootstrap JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<script src="{THEME}/js/libs.js"></script>

	</body>
</html>

